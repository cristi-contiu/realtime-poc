<?php
require __DIR__ . '/vendor/autoload.php';

$userID = @$_GET['user-id'] ?: null;
$token = RealTimeService::generateUserToken($userID);

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Firebase Sandbox</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="SitePoint">

  <script src="https://cdn.firebase.com/js/client/2.4.1/firebase.js"></script>

  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>

<body>
  <form name="sendAction" action="save.php" method="POST" onsubmit="return sendNotification(this);">
    Do action with user <input type="int" name="user" style="width:20px;" value="<?php echo $userID+1; ?>"> <button type="submit">Save</button>
  </form><br>
  <div id="notifications">
    <b>Notifications:</b><br>
  </div>

  <script>
    var userID = '<?php echo $userID; ?>'
        print = function(msg) { document.getElementById('notifications').innerHTML += msg + '<br>' },
        sendNotification = function(form) {
          var xhr = new XMLHttpRequest();
          xhr.open('GET', form.attributes.action.value + '?current-user=' + userID + '&target-user=' + form.user.value);
          xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE) {
              if (xhr.status === 200) {
                console.log(xhr.responseText); // 'This is the returned text.'
              } else {
                console.log('Error: ' + xhr.status); // An error occurred during the request.
              }
            }
          };
          xhr.send();
          return false;
        },
        userFBRef = new Firebase("<?php echo RealTimeService::FIREBASE_SERVER; ?>/users/" + userID );

    userFBRef.authWithCustomToken('<?php echo $token; ?>', function(error, authData) {
      if (error) {
        print("Login as " + userID + " Failed!");
        console.log("Login Failed!", error);
      } else {
        print("Login as " + userID + " Succeeded!");
        console.log("Login Succeeded!", authData);
      }
    });

    // userFBRef.child("notifications").on('child_added', function(snapshot) {
    //   var notification = snapshot.val();
    //   console.log('New notification', notification);
    //   print(notification.text + ' on ' + notification.date);
    // });
    // OR
    var displayedLastNotificationTime = Math.floor(Date.now() / 1000);
    userFBRef.child("last-notification-time").on('value', function(snapshot) {
      var lastNotificationTime = snapshot.val();
      console.log(lastNotificationTime, displayedLastNotificationTime);
      if (lastNotificationTime && lastNotificationTime > displayedLastNotificationTime) {
        console.log('New notification on ', lastNotificationTime);
        print('New notification, should read...');  
      }
    });
  </script>
</body>
</html>
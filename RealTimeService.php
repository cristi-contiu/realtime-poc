<?php

use Firebase\Token\TokenGenerator;
use Firebase\Token\TokenException;

class RealTimeService 
{
    const FIREBASE_SERVER = 'https://blinding-heat-6055.firebaseio.com';
    const FIREBASE_SECRET = 'Zit9FaAWdRyiiH2arfBI5YCM5XzWfx9exZyfCfEe';

    public static function generateUserToken($userID) 
    {
        try {
            $generator = new TokenGenerator(self::FIREBASE_SECRET);
            $token = $generator->setOption('debug', true)->setData(array('uid' => $userID));
            return $token->create();
        } catch (TokenException $e) {
          echo "Error: ".$e->getMessage();
        }
    }

    public static function sendNotification($notification) 
    {
        $url = self::FIREBASE_SERVER . '/users/' . $notification['user_id'] . '/notifications.json';
        $response = file_get_contents(
            $url . '?auth=' . self::FIREBASE_SECRET, 
            false, 
            stream_context_create(array(
                'http' => array(
                    'method' => 'POST',
                    'header' => "Content-Type: application/x-www-form-urlencoded; charset=utf-8" ,
                    'content' => json_encode($notification),
                    'ignore_errors' => true
                )
            ))
        );

        return array(
            'code' => $http_response_header,
            'body' => $response,
        );
    }

    public static function updateLastNotification($userID) 
    {
        $url = self::FIREBASE_SERVER . '/users/' . $userID . '/last-notification-time.json';
        $response = file_get_contents(
            $url . '?auth=' . self::FIREBASE_SECRET, 
            false, 
            stream_context_create(array(
                'http' => array(
                    'method' => 'PUT',
                    'header' => "Content-Type: application/x-www-form-urlencoded; charset=utf-8" ,
                    'content' => json_encode(time()),
                    'ignore_errors' => true
                )
            ))
        );

        return array(
            'code' => $http_response_header,
            'body' => $response,
        );
    }
}
# RealTime PoC

To test the real-time functionality:

0. Install with `composer install`
1. Start a local PHP webserver: `php -S localhost:8000`
2. Open the application as user 1: http://localhost:8000/?user-id=1
3. Open the application as user 2: http://localhost:8000/?user-id=2
4. As user 1, do a action with user 2 by pressing `Save`
5. In user's 2 tab, an instant text should appear: `New notification, should read...`

The `user-id` in steps 2 & 3 will come from the application's session.

##### RealTimeService class
* prepares tokens for logged-in users to authenticate on the realtime service
* connects the app to the realtime service and sends messages that are relayed to connected users' browsers.

##### Pusher
Pusher implementation can be accesed at http://localhost:8000/pusher.php?user-id=1.
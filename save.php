<?php

require __DIR__ . '/vendor/autoload.php';

$currentUser = @$_GET['current-user'];
$targetUser = @$_GET['target-user'];

// store action in database


// 1. create notification for target user
$notification = array(
    'user_id' => $targetUser,
    'text'    => "An action was done by user $currentUser",
    'url'     => 'http://link.to/new.action/',
    'date'    => date("Y-m-d H:i:s"),
);
// 2. save/persist notification in database

// 3. also send notification to realtime service
// RealTimeService::sendNotification($notification);
// OR
RealTimeService::updateLastNotification($targetUser);
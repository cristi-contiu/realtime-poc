<?php
$userID = @$_GET['user-id'] or die('No user-id specified. Use ?userid=1');
?>
<!DOCTYPE html>
<head>
  <title>Pusher Sandbox</title>
</head>
<body>
  <form name="sendAction" action="pusher-save.php" method="POST" onsubmit="return sendNotification(this);">
    Do action with user <input type="int" name="user" style="width:20px;" value="<?php echo $userID+1; ?>"> <button type="submit">Save</button>
  </form><br>
  <div id="notifications">
    <b>Notifications:</b><br>
  </div>

  <script src="https://js.pusher.com/3.0/pusher.min.js"></script>
  <script>
    var userID = '<?php echo $userID; ?>',
        print = function(msg) { document.getElementById('notifications').innerHTML += msg + '<br>' },
        sendNotification = function(form) {
          var xhr = new XMLHttpRequest();
          xhr.open('GET', form.attributes.action.value + '?current-user=' + userID + '&target-user=' + form.user.value);
          xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE) {
              if (xhr.status === 200) {
                console.log(xhr.responseText); // 'This is the returned text.'
              } else {
                console.log('Error: ' + xhr.status); // An error occurred during the request.
              }
            }
          };
          xhr.send();
          return false;
        };

    // Enable pusher logging - don't include this in production
    Pusher.log = function(message) {
      if (window.console && window.console.log) {
        window.console.log(message);
      }
    };

    var pusher = new Pusher('968300187de36d89b75a', {
      authEndpoint: 'pusher-auth.php',
      cluster: 'eu',
      encrypted: true
    });

    var channel = pusher.subscribe('private-user-<?php echo $userID; ?>');
    channel.bind('pusher:subscription_succeeded', function(data) {
      print('Connected as user <?php echo $userID; ?>.');
    });
    channel.bind('new_notification', function(data) {
      print('New notification, must read...');
    });
    channel.bind('new_message', function(data) {
      print('New message, must read...');
    });
  </script>
</body>
</html>
<?php
require __DIR__ . '/vendor/autoload.php';

// get current user id from session
function getCurrentUserID() {
    // for this demo, auth by channel name
    return explode("-", $_POST['channel_name'])[2];
}

if ( $_POST['channel_name'] == 'private-user-' . getCurrentUserID() ) {
    $pusher = new Pusher(
        '968300187de36d89b75a',
        '266b2a3d22c93d72d528',
        '190949',
        array( 'cluster' => 'eu', 'encrypted' => true )
    );
    echo $pusher->socket_auth($_POST['channel_name'], $_POST['socket_id']);
} else {
  header('', true, 403);
  echo "Forbidden";
}
<?php
require __DIR__ . '/vendor/autoload.php';

$currentUser = @$_GET['current-user'] or die('Missing current user!');
$targetUser = @$_GET['target-user'] or die('Missing target user!');


$pusher = new Pusher(
    '968300187de36d89b75a',
    '266b2a3d22c93d72d528',
    '190949',
    array( 'cluster' => 'eu', 'encrypted' => true )
);

$pusher->trigger("private-user-$targetUser", 'new_notification', array(
    'message' => "An action was done by user $currentUser",
    'url'     => 'http://link.to/new.action/',
    'date'    => date("Y-m-d H:i:s"),
));